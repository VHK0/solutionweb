@extends('layouts.app')

@section('content')

    <section class="content-header">
      <h1>{{Lang::get('core.invoices')}}</h1>
    </section>

  <div class="content">
      <div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-header-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left fa-2x"></i></a>
		</div>
	</div>
	<div class="box-body">
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
        @if ( \DB::table('bookings')->where('travellerID','=',app('request')->input('travellerID'))->count() ==0 && app('request')->input('travellerID') !='' )
        <div class="alert alert-danger alert-dismissible text-center">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="fa fa-warning fa-2x"></i> {{ Lang::get('core.alert') }}</h4>
                        {{ Lang::get('core.alertbooking') }}
        </div>
        @endif

		 {!! Form::open(array('url'=>'invoice/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
				{!! Form::hidden('invoiceID', $row['invoiceID']) !!}
									  <div class="form-group  " >
										<label for="Travellers" class=" control-label col-md-4 text-left"> {{Lang::get('core.travellers')}} <span class="asterix"> * </span></label>
										<div class="col-md-4">
										  <select name='travellerID' rows='5' id='travellerID' onchange="onchangetraveller()" class='select2 '
                                                  required  ></select>
										 </div>
										 <div class="col-md-4">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Booking ID" class=" control-label col-md-4 text-left"> {{Lang::get('core.bookingno')}}<span class="asterix"> * </span></label>
										<div class="col-md-4">
										  <select name='bookingID[]' multiple="" rows='5' id='bookingID' class='select2' required>
										  <option value="">-- Please Select --</option>
										  
										  </select>
										 </div>
										 <div class="col-md-4">
											
										 <button type="button" name="button" class="btn btn-success" id="tickets_modal_btn" data-toggle="modal" data-target="#tickets_modal"><i class="fa fa-plus"></i></button>
										 
										  <!-- <a href="javascript:void(0)" class="addC btn btn-success btn-sm tips" title="{{Lang::get('core.addnewproduct')}}" rel=".clone"> <i class="fa fa-plus"></i></a> -->
										 </div>
									  </div>
                                    <div class="form-group  " >
										<label for="Date Issued" class=" control-label col-md-4 text-left"> {{Lang::get('core.dateissued')}}<span class="asterix"> * </span></label>
										<div class="col-md-2">
 
				<div class="input-group m-b">
					{!! Form::text('DateIssued', $row['DateIssued'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
				</div>
										 </div>
										<label for="Due Date" class=" control-label col-md-2 text-left"> {{Lang::get('core.duedate')}} <span class="asterix"> * </span></label>
										<div class="col-md-2">

				<div class="input-group m-b">
					{!! Form::text('DueDate', $row['DueDate'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
				</div>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
    <div class="form-group  " >
										<label for="Accepted Payment Types" class=" control-label col-md-4 text-left">{{Lang::get('core.acceptedpayments')}} <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='payment_type[]' multiple rows='5' id='payment_type' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>


    <div class="table-responsive " style="padding: 20px; ">
		<table class="table table-striped ">
			<thead>
				<tr>
					<th width="150">{{Lang::get('core.productcode')}}</th>
					<th>{{Lang::get('core.product')}}</th>
					<th width="70"> {{Lang::get('core.qty')}} </th>
					<th width="70"> {{Lang::get('core.price')}}  </th>
					<th width="70"> {{Lang::get('core.total')}} </th>
					<th width="20"> </th>
				</tr>
			</thead>
			<tbody id ="table_edit">
			@if($row['invoiceID'] == '')
				<tr class="clone clonedInput">
					<input type="hidden" class="form-control" name="hidden_bookingID[]" id="hidden_bookingID">
					<td><input type="text" class="form-control" name="Code[]" id="productcode" placeholder="{{Lang::get('core.productcode')}}" required="false"></td>
					<td><input type="text" class="form-control" name="Items[]" id="itemname" placeholder="{{Lang::get('core.itemname')}} " required="true"></td>
					<td><input type="text" class="form-control" style="width: 70px;" id="qty" required="true" name="Qty[]" value=""></td>
					<td><input type="text" class="form-control" style="width: 70px;" id="amount" required="true" name="Amount[]"></td>
					<td><input type="text" class="form-control" style="width: 120px;" id="total" readonly="1" name="Total[]" > </td>
					<td>
					<a onclick=" $(this).parents('.clonedInput').remove(); calculateSum(); return false" href="javascript:void(0)" class="remove btn btn-xs btn-danger"><i class="fa fa-times" aria-hidden="true"></i>
          </a>
		  </td>
				</tr>
			@else
			<?php
			$temp = 0;
			?>
				@foreach ($items as $child)
				<tr class="clone clonedInput {{$temp !== '0' ? 'copy'.$temp : ''}}"> 
					<input type="hidden" class="form-control" name="hidden_bookingID[]" id="hidden_bookingID">
					<td><input type="text" class="form-control te" name="Code[]" id="productcode" placeholder="{{Lang::get('core.productcode')}}" required="true" value="{{ $child->Code}}"></td>
					<td><input type="text" class="form-control" name="Items[]" id="itemname" placeholder="{{Lang::get('core.itemname')}}" required="true" value="{{ $child->Items}}"></td>
					<td><input type="text" class="form-control" style="width: 70px;" id="qty" required="true" name="Qty[]" value="{{ $child->Qty}}"></td>
					<td><input type="text" class="form-control" style="width: 70px;" id="amount" required="true" name="Amount[]" value="{{ $child->Amount}}"></td>
					<td><input type="text" class="form-control" style="width: 120px;" id="total" readonly="1" name="Total[]" value="{{ $child->Qty * $child->Amount }}"> </td>
					<td>
					
					
					<a onclick=" $(this).parents('.clonedInput').remove(); calculateSum(); return false" href="javascript:void(0)" class="remove btn btn-xs btn-danger tips" title="{{Lang::get('core.btn_remove')}}"><i class="fa fa-times" aria-hidden="true"></i>
</a> 
				 	<input type="hidden" name="counter[]">
					</td>
				</tr>
				@php 
					$temp++;
				@endphp
				@endforeach
			@endif
				<tr> 
					<td colspan="3">
						<a href="javascript:void(0)" id="test" class="addC btn btn-success btn-sm tips" title="{{Lang::get('core.addnewproduct')}}" rel=".clone"> <i class="fa fa-plus"></i></a>
					</td>
					<td>{{Lang::get('core.subtotal')}}</td>
					<td><span id="SubtotalShow"></span><input type="hidden" class="form-control" style="width: 120px;" name="Subtotal"  value="{{ $row['Subtotal'] }}">
</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td>{{Lang::get('core.discount')}}</td>
					<td><input type="text" class="form-control" style="width: 120px;" name="discount" value="{{ $row['discount'] }}" ></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td>{{Lang::get('core.tax')}} (%)</td>
					<td><input type="text" class="form-control" style="width: 120px;" name="tax"  value="{{ $row['tax'] }}"></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td>{{Lang::get('core.total')}} </td>
					<td><span id="InvTotalShow"></span>

					<input type="hidden" class="form-control" style="width: 120px;" name="InvTotal"  value="{{ $row['InvTotal'] }}">
										 </td>
					<td width="100"><select name='currency' rows='3' id='currency' class='select2' required  ></select></td>
				</tr>
			</tbody>
		</table>


		</div>

									  <div class="form-group  " >
										<label for="Notes" class=" control-label col-md-4 text-left"> {{Lang::get('core.notes')}} </label>
										<div class="col-md-6">
										  <textarea name='notes' rows='5' id='notes' class='form-control '
				           >{{ $row['notes'] }}</textarea>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>

			</div>




			<div style="clear:both"></div>
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">
					<button type="submit" name="apply" class="btn btn-info btn-sm" > {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('invoice?return='.$return) }}' " class="btn btn-danger btn-sm ">  {{ Lang::get('core.sb_cancel') }} </button>
					</div>
				  </div>

		 {!! Form::close() !!}
	</div>
</div>

<div class="modal fade in" id="tickets_modal"  role="dialog" style=" padding-right: 16px;">
    <div class="modal-dialog" style="width:1200px;">
        <div class="modal-content">
            <div class="modal-header bg-default">
                <button type="button " class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">{{__('core.choose')}}</h4>
            </div>
            <div class="modal-body" id="edit-modal-content">
              <div class="table-responsive" style="min-height:300px; min-width:600px; padding-bottom:60px; border: none !important">
              
                  <table class="table table-striped " id="ticketsTable">
                      <thead>
                      <tr>
                      <!-- <th width="10"> No </th> -->
                      <th width="30"></th>
                      <th>{{Lang::get('core.bookings')}}</th>
                      <th>{{Lang::get('core.namesurname')}}</th>
                      <th>{{Lang::get('core.created')}}</th>
                      </tr>
                      </thead>
                      <tbody>
					  @php
					  $ii =0;
					  @endphp
                      	@foreach ($bookings as $rows)
								<tr class="editable booking_tr" id="form-{{$rows->bookingsID}}">
									<!-- <td class="number"> {{ ++$ii }}  </td> -->
									<td >
									<input type="checkbox" class="roomtype_ids roomtype_ids bookingIDs bookingID{{$rows->bookingsID}}" name="roomtype_ids[]" value="{{$rows->bookingsID}}" />  
									</td>
									<td data-values="" data-field="arrFlightNO">{{$rows->bookingno}}</td>
									<td data-values="" data-field="arrFlightNO">{{ \App\Library\SiteHelpers::formatLookUp($rows->travellerID,'travellerID','1:travellers:travellerID:nameandsurname') }}</td>
									<td data-values="" data-field="status">{{ \App\Library\SiteHelpers::TarihFormat($rows->created_at)}}
									</td>
								</tr>
          			   @endforeach
                    </tbody>
                  </table>
             

                <div class="form-group">
                    <label class="col-sm-5 text-right">&nbsp;</label>
                    <div class="col-sm-7">
                        <button type="button" id="storeBtns" class=" btn btn-success btn-sm"> Save </button>
						<!-- <a href="javascript:void(0)" class="addC btn btn-success btn-sm tips" title="{{Lang::get('core.addnewproduct')}}" rel=".clone"> <i class="fa fa-plus"></i></a> -->
						<input type="hidden" class="form-control" id="singleID" value="">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"> Cancel </button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>

<style>
span.select2-selection__choice__remove{
	display: none!important;
}
</style>
</div>

   <script type="text/javascript">
   var booking_all = [];
   var bookeds_length = [];
   var booked_all = [];
   var bookeds = [];
   var bookeds_length = "";
   var bookings_length = "";
   var travellerIDs_length = "";
   var traveller_IDs = [];
   var bookeds_cout = [];
   var traveller_booking_count = [];

   @php
      $i = 0;
   @endphp
   @foreach($bookings as $booking)
      booking_all[{{$i}}] = [];
      booking_all[{{$i}}]['bookingno'] = '{{$booking->bookingno}}';
      booking_all[{{$i}}]['travellerID'] = '{{$booking->travellerID}}';
      booking_all[{{$i}}]['created_at'] = '{{$booking->created_at}}';
      booking_all[{{$i}}]['bookingsID'] = '{{$booking->bookingsID}}';
      @php
        $i++;
      @endphp
   @endforeach
  

   @for ($i = 0; $i < $travellerIDs_length; $i++)
                          @php
                            $traveller_ID = $traveller_IDs[$i];
                            
						  @endphp
						  traveller_booking_count[{{$traveller_ID}}] = '{{$tranveller_bookeds_cout[$traveller_ID]}}';
                          booked_all[{{$traveller_ID}}] = [];
                    @if($bookeds_cout[$i] == 0)
                    @else
                            @for($y = 0; $y < $bookeds_cout[$i]; $y++)
							booked_all[{{$traveller_ID}}][{{$y}}] = '{{$bookeds[$traveller_ID][$y]->bookingsID}}'; 
                                
                              
                              @endfor
                    @endif
                   
    @endfor
	function onchangetraveller(){
		var travellerID = $("#travellerID").val();

		var tra_count = traveller_booking_count[travellerID];
		if(travellerID.length > 0){
			$("#tickets_modal_btn").show();
		}else{
			$("#tickets_modal_btn").hide();
		}
		var booking_all_length = booking_all.length;
		var booked_all_length = booked_all.length;
		for( var i = 0; i< booking_all_length; i++){
			// $("#form-"+booking_all[i]['bookingsID']).attr("style", "display: none;");
			$(".bookingID"+booking_all[i]['bookingsID']).iCheck('uncheck');
		}
		var temp_array = [];
		var merged_array = [];
		for( var y = 0; y< tra_count; y++){
			for( var i = 0; i< booking_all_length; i++){
							if(booking_all[i]['bookingsID'] !== booked_all[travellerID][y]){
							}else{

							temp_array.push(booking_all[i]['bookingsID']);
							merged_array.concat(temp_array);
								$("#form-"+booking_all[i]['bookingsID']).attr("style", "display: table-row;");
							}
					}
		}



	
   }

   

	
	$(document).ready(function() {
		$("span.select2-selection__choice__remove").remove();
		$("#bookingID").select2({ width:"100%" , dropdownParent: $('#mmb-modal-content')});
		// $("#tickets_modal_btn").hide();

		$("#tickets_modal_btn").click(function(){
		// $(".booking_tr").show();
		$(".bookingIDs").iCheck('uncheck');

			var bookingID_main = $("#bookingID").val();
		// $("#form-"+ bookingID_main).hide();
			

	});
	$(".remove").hide();

	var checked_bookingids = [];
		$('.bookingIDs').each(function(i, obj) {
		if(obj.checked)
		checked_bookingids.push(obj.value);

		});
	$("#storeBtns").click(function(){
		for(var remove = 1; remove < 11; remove++){
			$(".copy"+remove).remove();
			calculateSum();
		
		}
		
		var ids = [];
		$('.bookingIDs').each(function(i, obj) {
		if(obj.checked)
			ids.push(obj.value);

		});
		console.log(ids);
		console.log(ids);
		console.log(ids);
                  $("#bookingID").val(ids);
                  $("#bookingID").select2({ width:"100%" , dropdownParent: $('#mmb-modal-content')});
				  var length = ids.length;
				for(var jj = 0; jj < length; jj++){
					$("#test").click(); 
					console.log(length);
					var zz = jj+2;
							$.ajax({
							method: "POST",
							url: "{!! url('invoice/productfrom_bookingnsID') !!}",
							data: { bookingsID: ids[jj], singleID: jj}
						})
						.done(function( result ) {
							product = result['product'];
							// console.log(result);
							product = result['product'];
							singleID = result['singleID'];
							bookingsID = result['bookingsID'];
						if(singleID == "0"){
								$("#hidden_bookingID").val(bookingsID);
								$("#qty").val(1);
								$("#productcode").val(product['productcode']);
								$("#itemname").val(product['itemname']);
								$("#amount").val(product['amount']);
								var singleID = Number(singleID);
								calculateSum();
								$("#singleID").val(singleID);
								var sin = $("#singleID").val();
						}else{
							var singleID = Number(singleID)+1;
							// $(".remove").show();
							$("#hidden_bookingID"+singleID).val(bookingsID);
								$("#qty"+singleID).val(1);
								$("#productcode"+singleID).val(product['productcode']);
								$("#itemname"+singleID).val(product['itemname']);
								$("#amount"+singleID).val(product['amount']);
								var singleID = Number(singleID);
								calculateSum();
								$("#singleID").val(singleID);
								var sin = $("#singleID").val();
						}	

						});
				}
				$(".copy"+length).remove();
		$("#tickets_modal").modal('hide');
	});





    $("#travellerID").on("change", function(){

			$(".bookingIDs").iCheck('uncheck');
			if($("#select2-travellerID-results").html() != undefined){
				$("#qty").val("");
            $("#productcode").val("");
            $("#itemname").val("");
			$("#amount").val("");
			$("#bookingID").val("");
		console.log("===--------------===");

		console.log($("#bookingID").val());
			$("#bookingID").val("");
		console.log($("#bookingID").val());
		console.log("--------------===");
			
				for(var remove = 1; remove < 11; remove++){
					$(".copy"+remove).remove();
					calculateSum();
				}
			}

	});
    $("#bookingID").on("change", function(){
			
		// $("#tickets_modal_btn").show();
		console.log("result");
		// $("#tickets_modal_btn").hide();

      if($(this).val()!=""){
		   
							// var booking_ids = $(this).val();
							// var booking_ids_length = booking_ids.length;
							// console.log("ids");
							// console.log(booking_ids_length);
							// console.log("ids");
							// for(var jj = 0; jj < booking_ids_length; jj++){
							// 			$.ajax({
							// 				method: "POST",
							// 				url: "{!! url('invoice/product_from_bookingnsID') !!}",
							// 				data: { bookingsID: booking_ids[jj], singleID: booking_ids_length }
							// 			})
							// 			.done(function( result ) {
							// 				product = result['test'];
							// 				product = result['product'];
							// 				singleID = result['singleID'];
							// 				console.log(product);
							// 				console.log(result);
							// 				var length = singleID - 1;
							// 				$(this).remove();
							// 				$("#tickets_modal_btn").show();
							// 				$("#qty").val(1);
							// 				$("#productcode").val(product['productcode']);
							// 				$("#itemname").val(product['itemname']);
							// 				$("#amount").val(product['amount']);
							// 				$("#currency").val(product['currencyID']);
							// 				$("#currency").jCombo("{!! url('invoice/comboselect?filter=def_currency:currencyID:currency_sym|symbol&limit=WHERE:status:=:1') !!}",
							// 					{  selected_value : product['currencyID'] });
							// 				calculateSum();
							// 				console.log(product);
							// 			});
							// }
      }else{
		  console.log("-------");
		  $("#qty").val("");
            $("#productcode").val("");
            $("#itemname").val("");
            $("#amount").val("");
		  for(var remove = 1; remove < 11; remove++){
			$(".copy"+remove).remove();
			calculateSum();
		
		}
	  }
	 
    });

		$("#travellerID").jCombo("{!! url('invoice/comboselect?filter=travellers:travellerID:nameandsurname') !!}",
		{  selected_value : '@if ( app('request')->input('travellerID') != NULL ) {{app('request')->input('travellerID')}} @else {{ $row["travellerID"] }} @endif' });

		$("#bookingID").jCombo("{!! url('invoice/comboselect?filter=bookings:bookingsID:bookingno') !!}",
		{ selected_value : '{{ $row["bookingID"] }}'
    });

		$("#currency").jCombo("{!! url('invoice/comboselect?filter=def_currency:currencyID:currency_sym|symbol&limit=WHERE:status:=:1') !!}",
		{  selected_value : '{{ $row["currency"] }}' });

		$("#payment_type").jCombo("{!! url('invoice/comboselect?filter=def_payment_types:paymenttypeID:payment_type') !!}",
		{  selected_value : '{{ $row["payment_type"] }}' });


		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("invoice/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	});

function calculateSum()
{
	var Subtotal = 0;
	$('table tr.clone ').each(function(i){
		var Qty = $(this).find(" input[name*='Qty']").val();
		var Price = $(this).find("input[name*='Amount']").val();
		var sum = Qty * Price ;
		//alert( Qty +' + '+ Price + ' = '+ sum);
		Subtotal += sum;
	   $(this).find("input[name*='Total']").val(sum);
	})

	$('input[name=Subtotal]').val(Subtotal);

	var Discount 	= $('input[name=discount]').val();
	var Tax 		= $('input[name=tax]').val();

	var Total =  ( Subtotal - Discount ) +  (( Subtotal - Discount )*Tax/100)  ;
	$('input[name=InvTotal]').val(Total);
	$('#InvTotalShow').html(Total);
	$('#SubtotalShow').html(Subtotal)


}

	$(document).ready(function() {

		$('.addC').relCopy({});
 
		//$("input[name*='Total'] ").attr('readonly','1');
		$("input[name*='Qty'] , input[name*='Amount'] , input[name='discount'] , input[name='tax']").addClass('calculate');

		calculateSum();
		$(".calculate").keyup(function(){ calculateSum();})
		$('.remove').click(function(){ calculateSum()})

	});
	</script>

@stop
