@extends('layouts.app')
@section('content')
<style>
.creategroup:hover {
  color: #337ab7;
}
</style>
<section class="content-header page-title">
                <h1>
                    Boards <small></small>
                </h1>
                <ol class="breadcrumb" style="position: inherit;">
                    <li><a href="{{ url('bookinggroups?return=') }}"><i
                                class="fas fa-tachometer-alt"></i> Groups</a></li>
                    <li class="active"> View </li>
                    <li class="active"> 9 </li>
                </ol>
            </section>
           <a href="#"> <section class="content-header page-title mt-30" style="margin-top: 40px!important;">
          
                <h1 style="text-align: center;"  class="creategroup">CREATE A GROUP
                </h1>
            </section></a>
            <section class="content">

                <div class="row">

                    <div class="col-md-12" data-layout-box="53">
                        <div class="box box  ">
                            <div class="box-body view ">
                            <link rel="stylesheet" href="{{ asset('assets')}}/template/custom.css">
                                    <div id="planner-container" class="limited planner-boards">
                                    <div class="planner-header">
                                    @foreach($guides as $guide)
                                        <div class="column" style="background-color:#3c40c6">
                                            <h5 data-toggle="tooltip" title="{{$guide->name}}">{{$guide->name}}</h5>
                                        </div>
                                    @endforeach
                                    </div>
                                    <div class="planner-body">
                                        @foreach($guides as $guide)
                                            <div class="column sortable" data-update="column" data-column="20">
                                                <div data-task="14" class="task-box" style="background-color:#3c40c6">
                                                    <div class="task-inner">
                                                        <div class="left pull-left">
                                                            <div class="task-head">
                                                                <div class="task-title" data-toggle="tooltip"
                                                                    title="M32 facebook promotions">
                                                                    <a href="#"
                                                                        target="_blank">
                                                                        M32 facebook promotions </a>
                                                                </div>
                                                                <a class="project" style="font-size: 1.2em;" href=""></a>
                                                                <small class="label label-warning">Working on</small>

                                                            </div>
                                                            <div class="task-body">
                                                                <div class="text">Facebook posts for M32 with 2 photos
                                                                    (interior and some products) for promoting the
                                                                    brand&#8230;</div>
                                                            </div>
                                                            <div class="task-foot">
                                                                <div class="dates">
                                                                    <strong>Closed at:</strong>
                                                                    -<br />

                                                                    <strong>Deadline:</strong>
                                                                    <span class="expired">05/12/2019</span> <img
                                                                        src="data:image/gif;base64,R0lGODlhMgAsAEcAACH/C05FVFNDQVBFMi4wAwEFAAAh+QQAZAAAACwAAAAAMgAsAIPgSk/wnZ/vwcTz4eTKdYXLmandytXm2eHEtsrHzuHT2Of3+Prf5vHo7/fuGBf///8E+/DJSatdxGkyrP9gOAlaWSxiqlZYqQHGKqeBWxLorFcDYL+xnfCRufleuaFs4CIkipqCcgl1IBaMowNwmKpqNwUKHE16PQstANGQNNTdM5rsKDCSpFtbXhGoE3sSLSUCfBQLdHYDAowdBi4AgXx5G2IFWw44D4mGD2kubIg3d28lXIaUdWKDWwoSdJpnPaZsnmROglAwcokMgmqunqmxSrMvgBO3CROiJlOsVr7JN8sTB5DSQsMJZrfBgpxCny+1FJQE2W5qQTqw3yOjFrBmKsZb5RQHR3YWDbrsK6rws6BvSwFJ8EwhDLGNngRjB+eAkiEwHYVSaxZKKPgizY6IYWI+DMiwJgSdAA5ZqMF3wQcAbiBKvXjnAdYdkwAIXGHYRONDSMhCDChQgKaHKoU+VEyBRcHNEBy3WJQAMqU5AliThoB2QiUonxRu1VNjsSqNeCkumUhi7yVYcwDiolMxbkuQZlGMfligAIHfqR8emRrTBOaKBQ0YNLDqQW0ZLVsCSJ5MubLly5gDVAHA5Ifnz6BDe+YCWbTp054JKPCDurVrAPwOFCAwG6vt27hz695tuzaCp4gbCFesOPFw48iJK0+emPjx4osXRAAAIfkEAGQAAAAsAAAAADIALACD4EpP8J2f78HE8+HkynWFy5mp3crV5tnhxLbKx87h09jn9/j63+bx6O/37hgX////BDTwyUmrvTjrzbv/YCiOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSCwaj8ikcslsOp/Q6DECACH5BABkAAAALAAAAAAyACwAg+BKT/Cdn+/BxPPh5Mp1hcuZqd3K1ebZ4cS2ysfO4dPY5/f4+t/m8ejv9+4YF////wT78MlJq13EaTKs/2A4CVpZLGKqVlipAcYqp4FbEuisVwNgv7Gd8JG5+V65oWzgIiSKmoJyCXUgFoyjA3CYqmo3BQocTXo9Cy0A0ZA01N0zmuwoMJKkW1teEagTexItJQJ8FAt0dgMCjB0GLgCBfHkbYgVbDjgPiYYPaS5siDd3byVchpR1YoNbChJ0mmc9pmyeZE6CUDByiQyCaq6eqbFKsy+AE7cJE6ImU6xWvsk3yxMHkNJCwwlmt8GCnEKfL7UUlATZbmpBOrDfI6MWsGYqxlvlFAdHdhYNuuwrqvCzoG9LAUnwTCEMsY2eBGMH54CSITAdhVJrFkoo+CLNjohhYj4MyLAmBJ0ADlmowXfBBwBuIEq9eOcB1h2TAAhcYdhE40NIyEIMKFCApocqhT5UTIFFwc0QHLdYlAAypTkCWJOGgHZCJSifFG7VU2OxKo14KS6ZSGLvJVhzAOKiUzFuS5BmUYx+WKAAgd+pHx6ZGtME5ooFDRg0sOpBbRktWwJInky5suXLmANUAcDkh+fPoEN75gJZtOnTngko8IO6tWsA/A4UIDAbq+3buHPr3m27NoKniBsIV6w48XDjyIkrT56Y+PHiixdEAAAh/rZGSUxFIElERU5USVRZDQpDcmVhdGVkIG9yIG1vZGlmaWVkIGJ5DQpLRUlUSCBTT0FSRVMNCihLZWl0aCBNLiBTb2FyZXMpDQpCZWFuIENyZWF0aXZlDQoNCkNyZWF0ZWQgYnkgQWxjaGVteSBNaW5kd29ya3MnDQpHSUYgQ29uc3RydWN0aW9uIFNldCBQcm9mZXNzaW9uYWwNCmh0dHA6Ly93d3cubWluZHdvcmtzaG9wLmNvbQAh/wtHSUZDT050YjEuMAIBAA4KAAYAAwAAAAAAAAAAAApDbGlwYm9hcmQAADs="
                                                                        class="warning-icon" />
                                                                </div>

                                                                <div class="actions text-right">




                                                                    <a style="color:#ffffff;background-color:purple"
                                                                        href="https://personal-kanban-board.firegui.com/get_ajax/modal_form/kanban-form-task/14"
                                                                        class="btn btn-xs purple js_open_modal"
                                                                        data-csrf="eyJuYW1lIjoiY3NyZl90b2tlbl80YTQ0MzEyMmE3NGEyMTY0ODFhN2M2NTAxYzM4Njk4YyIsImhhc2giOiJkOWM3NDEzNGU2NTc3ZjhhMmI4MjY1ODU3NmNhZGNmMyJ9"><i
                                                                            class="fas fa-edit"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="right pull-left">
                                                            <div class="photos">
                                                                <a href="#">
                                                                    <img class="user-avatar"
                                                                        src="{{ asset('uploads/images/4797-1') }}"
                                                                        data-toggle="tooltip" alt="John" title="John"
                                                                        width='40' />
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Add new task -->
                                                <div class="add_new_task">
                                                    <a href="https://personal-kanban-board.firegui.com/get_ajax/modal_form/kanban-form-task?tasks_column=20&tasks_users=1"
                                                        class="js_open_modal small-box-footer"
                                                        data-csrf="eyJuYW1lIjoiY3NyZl90b2tlbl80YTQ0MzEyMmE3NGEyMTY0ODFhN2M2NTAxYzM4Njk4YyIsImhhc2giOiJkOWM3NDEzNGU2NTc3ZjhhMmI4MjY1ODU3NmNhZGNmMyJ9"><i
                                                            class="fas fa-plus"></i></a>
                                                </div>
                                                <!-- End new task -->
                                            </div>
                                         @endforeach
                                    </div>
                                </div>


                                <script
                                    src="https://personal-kanban-board.firegui.com/modulesbridge/loadAssetFile/personal-kanban-board?file=js/planner.js&v=1.9.2">
                                </script>
                            </div>
                        </div>

                    </div>

                </div>
             


                <style>
                .js_topbar_timetracker {
                    float: right;
                    padding: 13px 20px 0 0;
                }

                .js_topbar_task_title {
                    background: #e4e4e4;
                    padding: 4px;
                    font-size: 0.9em;
                    border-radius: 3px;
                    margin-right: 10px;
                }

                .js_topbar_task_title.working_on {
                    background: #f39c12 !important;
                }
                </style>
                <div class="js_topbar_timetracker_container" style="display:none">

                    <div class="js_topbar_timetracker">
                        <span class="js_topbar_task_title">Resume: My first task</span>
                        <a href="https://personal-kanban-board.firegui.com/personal-kanban-board/main/task_working_on/1/1"
                            style="background-color:green;color:#ffffff" class="btn btn-xs green js_link_ajax"
                            data-toggle="tooltip" title="Start time tracker"><i class="fa fa-play"></i></a>

                    </div>
                </div>

                <script>
                console.log($('.js_topbar_timetracker', '.navbar').length);

                if ($('.js_topbar_timetracker', '.navbar').length == 0) {
                    $('.navbar').append($('.js_topbar_timetracker_container').html());
                }

                var minutesLabel = document.getElementById("minutes");
                var secondsLabel = document.getElementById("seconds");
                var hoursLabel = document.getElementById("hours");


                function setTime() {
                    ++totalSeconds;
                    secondsLabel.innerHTML = pad(totalSeconds % 60);
                    minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
                    hoursLabel.innerHTML = pad(parseInt(totalSeconds / 60 / 60));
                }

                function pad(val) {
                    var valString = val + "";
                    if (valString.length < 2) {
                        return "0" + valString;
                    } else {
                        return valString;
                    }
                }
                </script>

                <script>
                $(document).ready(function() {
                    $('body').addClass('page-sidebar-closed').find('.page-sidebar-menu').addClass(
                        'page-sidebar-menu-closed');
                });
                </script>

            </section>
@stop
