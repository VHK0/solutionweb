
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/bootstrap.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/all.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/bootstrap.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/ionicons.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/jquery-jvectormap.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/bootstrap-datepicker.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/bootstrap-datetimepicker.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/bootstrap-timepicker.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/daterangepicker.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/bootstrap3-wysihtml5.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/dataTables.bootstrap.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/select2.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/ion.rangeSlider.Metronic.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/components-md.css.map?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/timeline.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/fullcalendar.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/bootstrap-colorselector.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/jquery-ui.theme.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/AdminLTE.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/_all-skins.min.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/bootstrap-fileinput.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/lightbox.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/jquery.fancybox.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/dropzone.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/leaflet.css?v=1.9.2" />
    <link rel="stylesheet" href="{{ asset('assets')}}/template/leaflet.draw.css?v=1.9.2" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/template/custom.css?v=1.9.2" />
    <link rel="shortcut icon" href="https://personal-kanban-board.firegui.com/favicon.ico" />
    <style>
        .main-header .sidebar-toggle {
            float: left;
            background-color: transparent;
            background-image: none;
            padding: 15px 15px;
            /* font-family: fontAwesome; */
            font-family: "Font Awesome\ 5 Free";
            /* cjr */
        }
        .main-header .sidebar-toggle:before {
            content: "\f0c9";
            font-weight: 900;
            /* cjr */
        }
    </style>
    <script>
        var base_url = "https:\/\/personal-kanban-board.firegui.com\/";
        var base_url_admin = "https:\/\/personal-kanban-board.firegui.com\/";
        var base_url_template = "https:\/\/personal-kanban-board.firegui.com\/";
        var base_url_scripts = "https:\/\/personal-kanban-board.firegui.com\/";
        var base_url_uploads = "https:\/\/personal-kanban-board.firegui.com\/";
        var lang_code = 'en-EN';
        var lang_short_code = 'en';
    </script>
    <script src="{{ asset('assets')}}/script/moment.min.js?v=1.9.2"></script>
    <script src="{{ asset('assets')}}/script/jquery.min.js?v=1.9.2"></script>
    <script src="{{ asset('assets')}}/script/jquery-migrate-3.0.0.min.js?v=1.9.2"></script>
    <script src="{{ asset('assets')}}/script/highcharts.js?v=1.9.2"></script>
    <script src="{{ asset('assets')}}/script/exporting.js?v=1.9.2"></script>
    <script src="{{ asset('assets')}}/script/funnel.js?v=1.9.2"></script>
    <script src="{{ asset('assets')}}/script/Chart.js?v=1.9.2"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/script/Chart.css?v=1.9.2" />
    <script src="{{ asset('assets')}}/script/apexcharts.min.js?v=1.9.2"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets')}}/script/apexcharts.css?v=1.9.2" />
    <script>
        $(function() {
            Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
                return {
                    radialGradient: {
                        cx: 0.5,
                        cy: 0.3,
                        r: 0.7
                    },
                    stops: [
                        [0, color],
                        [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                    ]
                };
            });
        });
    </script>