        <!-- COMMON PLUGINS -->
        <script src="{{ asset('assets/script/jquery-ui.min.js?v=1.9.2') }}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ asset('assets/script/bootstrap.min.js?v=1.9.2') }}"></script>
        <!-- Morris.js charts -->
        <script src="{{ asset('assets/script/raphael.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/jquery.sparkline.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/jquery-jvectormap-1.2.2.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/jquery-jvectormap-world-mill-en.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/jquery.knob.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/moment.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/daterangepicker.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/bootstrap-datepicker.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/bootstrap3-wysihtml5.all.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/jquery.slimscroll.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/fastclick.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/adminlte.min.js?v=1.9.2') }}"></script>
        <!-- DataTables -->
        <script src="{{ asset('assets/script/jquery.dataTables.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/dataTables.bootstrap.min.js?v=1.9.2') }}"></script>
        <!-- INPUT DATA PLUGINS -->
        <script src="{{ asset('assets/script/fullcalendar.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/lang-all.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/jquery.blockui.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/jquery.cokie.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/uniform/jquery.uniform.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/select2.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/i18n/it.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/bootstrap-fileinput.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/bootstrap-datepicker.it.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/bootstrap-timepicker.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/bootstrap-colorpicker.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/bootstrap-datetimepicker.min.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/bootstrap-datetimepicker.it.js?v=1.9.2') }}"></script>
        <!-- OUTPUT DATA PLUGINS -->
        <script src="{{ asset('assets/script/bootstrap-hover-dropdown.min.js?v=1.9.2') }}"></script>
        <!-- LIBRARIES -->
        <script src="{{ asset('assets/script/ckeditor.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/jquery.fancybox.pack.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/storyjs-embed.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/lightbox.min.js?v=1.9.2') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/script/bootstrap-colorselector.min.js?v=1.9.2') }}"></script>
        <!-- LEAFLET-JS -->
        <script src="{{ asset('assets/script/leaflet.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/leaflet.geocoding.js?v=1.9.2') }}"></script>
        <!-- LEAFLET-DRAW-LIBRARY-JS !-->
        <script src="{{ asset('assets/script/Leaflet.draw.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Leaflet.Draw.Event.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Toolbar.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Tooltip.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/GeometryUtil.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/LatLngUtil.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/LineUtil.Intersect.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Polygon.Intersect.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Polyline.Intersect.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/TouchEvents.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/DrawToolbar.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Draw.Feature.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Draw.SimpleShape.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Draw.Polyline.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Draw.Marker.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Draw.Circle.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Draw.CircleMarker.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Draw.Polygon.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Draw.Rectangle.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/EditToolbar.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/EditToolbar.Edit.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/EditToolbar.Delete.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Control.Draw.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Edit.Poly.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Edit.SimpleShape.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Edit.Rectangle.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Edit.Marker.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Edit.CircleMarker.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/Edit.Circle.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/dropzone.js') }}"></script>
        <script>
            Dropzone.autoDiscover = false;
        </script>
        <!-- ADMINLTE SCRIPTS -->
        <script src="{{ asset('assets/script/adminlte_components.js?v=1.9.2') }}"></script>
        <!-- CUSTOM COMPONENTS -->
        <script src="{{ asset('assets/script/submitajax.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/ajax-tables.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/datatable_inline.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/data-tables.js?v=1.9.2') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/script/bulk-grid.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/components.js?v=1.9.2') }}"></script>
        <script src="{{ asset('assets/script/IE.missing.functions.js?v=1.9.2') }}"></script>